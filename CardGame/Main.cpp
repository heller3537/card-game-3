// Ryan Appel

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN,
	EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit { SPADE, HEART, DIAMOND, CLUB };

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card);
Card HighCard(Card c1, Card c2);

int main()
{
	Card c1;
	c1.rank = EIGHT;
	c1.suit = CLUB;

	Card c2;
	c2.rank = TEN;
	c2.suit = SPADE;

	PrintCard(c1);
	cout << endl;

	PrintCard(HighCard(c1, c2));

	_getch();
	return 0;
}

Card HighCard(Card c1, Card c2)
{
	if (c1.rank >= c2.rank) return c1;
	if (c1.rank < c2.rank) return c2;
}

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
	case TWO: cout << "two"; break;
	case THREE: cout << "three"; break;
	case FOUR: cout << "four"; break;
	case FIVE: cout << "five"; break;
	case SIX: cout << "six"; break;
	case SEVEN: cout << "seven"; break;
	case EIGHT: cout << "eight"; break;
	case NINE: cout << "nine"; break;
	case TEN: cout << "ten"; break;
	case JACK: cout << "jack"; break;
	case QUEEN: cout << "queen"; break;
	case KING: cout << "king"; break;
	case ACE: cout << "ace"; break;
	}

	cout << " of ";
	switch (card.suit)
	{
	case SPADE: cout << "spades"; break;
	case HEART: cout << "hearts"; break;
	case DIAMOND: cout << "diamonds"; break;
	case CLUB: cout << "clubs"; break;

	}
}